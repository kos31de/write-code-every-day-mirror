require_relative "../lib/test_helper"
require_relative "../answers/exercise_13"

Test.suite(
  settings: [Setting2],
  answers: [Answer1],
  expected_value: {name: ["田中", nil, "太郎"], address: ["東京都", "港区", "芝浦"], age: [20], membership: [false]}
)

Test.suite(
  settings: [Setting1],
  answers: [Answer2, Answer3],
  expected_value: ["×", "×", "○", "○", "○", "×", "×", "○","×"]
)
