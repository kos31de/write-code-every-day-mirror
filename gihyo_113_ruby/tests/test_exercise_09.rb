require_relative "../lib/test_helper"
require_relative "../answers/exercise_09"

Test.suite(
  answers: [Answer1, Answer2, Answer3],
  expected_value: "03"
)
