require_relative "../lib/test_helper"
require_relative "../answers/exercise_29"

Test.suite(
  answers: [Answer1, Answer2],
  expected_value: ["foobar", "foo", "foo"]
)
