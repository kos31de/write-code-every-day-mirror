require_relative "../lib/test_helper"
require_relative "../answers/exercise_02"

Test.suite(
  answers: [Answer1, Answer2, Answer3],
  expected_output: "10\n11\n12\n13\n14\n15\n16\n17\n18\n19\n20\n"
)