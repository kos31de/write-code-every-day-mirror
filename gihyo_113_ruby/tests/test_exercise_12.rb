require_relative "../lib/test_helper"
require_relative "../answers/exercise_12"

Test.suite(
  settings: [Setting1],
  answers: [Answer1, Answer3, Answer5],
  expected_value: [1, 1, 2, 3, 5, 8, 13, 21, 34, 55]
)

Test.suite(
  settings: [Setting2],
  answers: [Answer2, Answer4, Answer6],
  expected_value: {1=>1, 2=>1, 3=>2, 4=>3, 5=>5, 6=>8, 7=>13, 
    8=>21, 9=>34, 10=>55}
)
