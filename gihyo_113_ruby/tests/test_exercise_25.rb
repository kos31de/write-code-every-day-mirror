require_relative "../lib/test_helper"
require_relative "../answers/exercise_25"

Test.suite(
  settings: [Setting1],
  answers: [Answer1, Answer2, Answer3, Answer4, Answer5],
  expected_value: "Fizz"
)

Test.suite(
  settings: [Setting2],
  answers: [Answer1, Answer2, Answer3, Answer4, Answer5],
  expected_value: "FizzBuzz"
)

Test.suite(
  settings: [Setting3],
  answers: [Answer1, Answer2, Answer3, Answer4, Answer5],
  expected_value: 2
)
