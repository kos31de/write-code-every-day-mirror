require_relative "../lib/test_helper"
require_relative "../answers/exercise_14"

Test.suite(
  settings: [Setting1],
  answers: [Answer1, Answer2, Answer3],
  expected_value: ["English", "Hindi"]
)
