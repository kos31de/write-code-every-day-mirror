require_relative "../lib/test_helper"
require_relative "../answers/exercise_34"

Test.suite(
  answers: [Answer1],
  expected_value: [3, 1, 1, 0, 0, 4, 1, 3, 0, 2]
)
