require_relative "../lib/test_helper"
require_relative "../answers/exercise_19"

Test.suite(
  settings: [Setting1],
  answers: [Answer1],
  expected_value: [1, 3, 3, 9, 13, 14, 15, 16, 17, 18]
)

Test.suite(
  settings: [Setting1],
  answers: [Answer2, Answer4],
  expected_value: [18, 17, 16, 15, 14, 13, 9, 3, 3, 1]
)

Test.suite(
  settings: [Setting1],
  answers: [Answer3, Answer5],
  expected_value: [3, 3, 16, 9, 15, 17, 14, 1, 18, 13]
)
