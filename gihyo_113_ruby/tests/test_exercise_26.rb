require_relative "../lib/test_helper"
require_relative "../answers/exercise_26"

Test.suite(
  answers: [Answer1, Answer5],
  expected_value: 25
)

Test.suite(
  answers: [Answer2, Answer6],
  expected_value: false
)

Test.suite(
  answers: [Answer3, Answer7],
  expected_value: true
)

Test.suite(
  settings: [Setting1],
  answers: [Answer4, Answer8],
  expected_value: true
)
