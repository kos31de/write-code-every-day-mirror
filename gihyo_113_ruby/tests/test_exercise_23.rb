require_relative "../lib/test_helper"
require_relative "../answers/exercise_23"

Test.suite(
  settings: [Setting1],
  answers: [Answer1, Answer2],
  expected_truth_value: false
)

Test.suite(
  settings: [Setting2],
  answers: [Answer1, Answer2],
  expected_truth_value: false
)

Test.suite(
  settings: [Setting3],
  answers: [Answer1, Answer2],
  expected_truth_value: true
)

Test.suite(
  settings: [Setting4],
  answers: [Answer1, Answer3, Answer4],
  expected_truth_value: false
)

Test.suite(
  settings: [Setting5],
  answers: [Answer1, Answer3, Answer4],
  expected_truth_value: false
)

Test.suite(
  settings: [Setting6],
  answers: [Answer1, Answer3, Answer4],
  expected_truth_value: true
)
