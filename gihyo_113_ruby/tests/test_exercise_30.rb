require_relative "../lib/test_helper"
require_relative "../answers/exercise_30"

Test.suite(
  answers: [Answer1, Answer2, Answer3, Answer4],
  expected_value: "(a) (b) (c)"
)
