require_relative "../lib/test_helper"
require_relative "../answers/exercise_28"

Test.suite(
  answers: [Answer1, Answer2],
  expected_value: {
    "id1 == id2" => false,
    "id1 == id1" => true,
    "id2 == id2" => true,
  }
)
