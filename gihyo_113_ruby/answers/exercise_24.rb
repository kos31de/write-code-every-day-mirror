module Context1
  def foo(i); sleep 1; %w[foo bar baz][i] end
end

module Context2
  def foo(i); sleep 1; %w[foo bar baz][i % 3] end
end

module Setting1
  def N; @N ||= 2 end
end

module Setting2
  def N; @N ||= 3 end
end

module Setting3
  def REPEAT; @REPEAT ||= 4 end
end

module Answer1
  def bar; foo($N) end

  def code
    $N = N()
    REPEAT().times{bar}
  end
end

module Answer2
  def bar; @foo ||= foo($N) end

  def code
    $N = N()
    REPEAT().times{bar}
  end
end

module Answer3
  def bar
    instance_variable_defined?(:@foo) ? @foo : @foo = foo($N)
  end

  def code
    $N = N()
    REPEAT().times{bar}
  end
end
