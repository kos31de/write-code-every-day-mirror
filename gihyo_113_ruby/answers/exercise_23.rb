module Setting1
  def error; @error ||= nil end
end

module Setting2
  def error; @error ||= "" end
end

module Setting3
  def error; @error ||= "foo" end
end

module Setting4
  def error; @error ||= nil end
end

module Setting5
  def error; @error ||= {} end
end

module Setting6
  def error; @error ||= {a: "foo"} end
end

module Answer1
  def code
    error&.empty?
  end
end

module Answer2
  def code
    !error.to_s.empty?
  end
end

module Answer3
  def code
    !error.to_h.empty?
  end
end

module Answer4
  def code
    error&.any?
  end
end
