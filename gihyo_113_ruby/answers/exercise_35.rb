module Setting1
  def array; @array ||= ["a", "b", "c", "d", "b", "c", "a", "c"] end
end

module Answer1
  def code
    array.each_with_object({}).with_object({}) do
      |(elem, first_occurrences), second_occurrences|
      if !first_occurrences.key?(elem)
        first_occurrences[elem] = true
      elsif !second_occurrences.key?(elem)
        second_occurrences[elem] = true
      end
    end.keys
  end
end
