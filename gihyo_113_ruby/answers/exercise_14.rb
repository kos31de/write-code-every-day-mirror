module Setting1
  def languages
    @languages ||= ["Mandarin", "Spanish", "English", "Hindi", "Arabic"]
  end
end

module Answer1
  def code
    languages[2..3]
  end
end

module Answer2
  def code
    languages[2...4]
  end
end

module Answer3
  def code
    languages[2, 2]
  end
end
