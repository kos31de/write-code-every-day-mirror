module Setting1
  def n; @n ||= 100 end
  def max; @max ||= Math.sqrt(n).floor end
end

module Answer1
  def code
    (2..max).each_with_object((2..n).to_a) do |i, array|
      array.reject!{|j| j % i == 0 unless j == i}
    end
  end
end
