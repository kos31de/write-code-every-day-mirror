module Setting1
  def arrays; @arrays ||= [%w[a b c], %w[b d], %w[a c]] end
end

module Answer1
  def code
    first, *rest = arrays
    first.union(*rest)
  end
end

module Answer2
  def code
    [].union(*arrays)
  end
end

module Answer3
  def code
    :union.to_proc.call(*arrays)
  end
end
