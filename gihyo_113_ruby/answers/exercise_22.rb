module Setting1
  def hash
    @hash ||= {"a" => 1, "A" => 2, "B" => 2, "c" => 3, "C" => 1}
  end
end

module Answer1
  def code
    hash.each_with_object({}){|(k, v), h|
      h[k.downcase] += v
    }
  end
end

module Answer2
  def code
    hash.each_with_object(Hash.new(0)){|(k, v), h|
      h[k.downcase] += v
    }
  end
end
