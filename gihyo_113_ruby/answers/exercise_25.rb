module Setting1
  def n; @n ||= 6 end
end

module Setting2
  def n; @n ||= 45 end
end

module Setting3
  def n; @n ||= 2 end
end

module Answer1
  def fizzbuzz(n)
    if n % 3 == 0 then "Fizz"
    elsif n % 5 == 0 then "Buzz"
    elsif n % 15 == 0 then "FizzBuzz"
    else n
    end
  end

  def code
    fizzbuzz(n)
  end
end

module Answer2
  def fizzbuzz(n)
    if n % 3 != 0 and n % 5 != 0 then n
    elsif n % 3 == 0 and n % 5 != 0 then "Fizz"
    elsif n % 3 != 0 and n % 5 == 0 then "Buzz"
    elsif n % 3 == 0 and n % 5 == 0 then "FizzBuzz"
    end
  end

  def code
    fizzbuzz(n)
  end
end

module Answer3
  def fizzbuzz(n)
    s = ""
    s << "Fizz" if n % 3 == 0
    s << "Buzz" if n % 5 == 0
    s.empty? ? n : s
  end

  def code
    fizzbuzz(n)
  end
end

module Answer4
  def fizzbuzz(n)
    if n % 15 == 0 then "FizzBuzz"
    elsif n % 3 == 0 then "Fizz"
    elsif n % 5 == 0 then "Buzz"
    else n
    end
  end

  def code
    fizzbuzz(n)
  end
end

module Answer5
  def fizzbuzz(n)
    return "FizzBuzz" if n % 15 == 0
    return "Fizz" if n % 3 == 0
    return "Buzz" if n % 5 == 0
    n
  end

  def code
    fizzbuzz(n)
  end
end
