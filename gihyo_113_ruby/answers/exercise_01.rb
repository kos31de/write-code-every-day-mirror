module Answer1
  def code
    100 * 1.1 == 110
  end
end

module Answer2
  def code
    (100 * 1.1).round == 110
  end
end

module Answer3
  def code
    100 * 1.1r == 110
  end
end
