module Answer1
  def generate_method(name)
    define_method(name){rand}
  end

  def code
    self.class.generate_method(:id1)
    self.class.generate_method(:id2)
    first_id1 = id1
    second_id1 = id1
    first_id2 = id2
    second_id2 = id2
    {
      "id1 == id2" => first_id1 == first_id2,
      "id1 == id1" => first_id1 == second_id1,
      "id2 == id2" => first_id2 == second_id2
    }
  end
end

module Answer2
  def generate_method(name)
    parameter = rand
    define_method(name){parameter}
  end

  def code
    self.class.generate_method(:id1)
    self.class.generate_method(:id2)
    first_id1 = id1
    second_id1 = id1
    first_id2 = id2
    second_id2 = id2
    {
      "id1 == id2" => first_id1 == first_id2,
      "id1 == id1" => first_id1 == second_id1,
      "id2 == id2" => first_id2 == second_id2
    }
  end
end
