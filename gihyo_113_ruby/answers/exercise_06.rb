module Setting1
  def s; @s ||= "llll1lll" end
end

module Answer1
  def code
    raise "No numbers please" unless s.match?(/\A\D+\z/)
  end
end

module Answer2
  def code
    raise "No numbers please" if s.match?(/\d/)
  end
end
