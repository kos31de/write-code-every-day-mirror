module Answer1
  def code
    "a b c".gsub(/\w/, "(#$&)")
  end
end

module Answer2
  def code
    "a b c".gsub(/\w/){"(#$&)"}
  end
end

module Answer3
  def code
    "a b c".gsub(/\w/){|s| "(#{s})"}
  end
end

module Answer4
  def code
    "a b c".gsub(/\w/, "(\\&)")
  end
end
