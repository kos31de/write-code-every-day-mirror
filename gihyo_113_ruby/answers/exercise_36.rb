module Setting1
  def array; @array ||= ["hello", "world"] end
end

module Answer1
  def code
    array.map{|x| x.capitalize}
  end
end

module Answer2
  def code
    array.map(&:capitalize)
  end
end
