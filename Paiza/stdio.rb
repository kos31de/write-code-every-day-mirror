# 標準入力とループ処理
count = gets.to_i
puts("データ個数 #{count}")

for i in 1..count
line = gets
puts "hello #{line}"
end

# 1行目に取得対象のデータ数、２行目以降はarrayに格納
# doはなくてもいい？
# \Rは広い範囲で改行になる文字全てにマッチする。\rと比較して改行コードがバラバラでも大丈夫。
num = gets.to_i
array = []
while s = gets do
  array << s.sub(/\R/,"")
end
p array


# 1行目に取得データ数、2行目位以降各行で複数の値が入力される
