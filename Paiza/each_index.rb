# 配列の要素の数だけ繰り返す
animals = ["dog", "cat", "mouse"]
animals.each_index {|idx| print "#{idx}. #{animals[idx]}" }
# => 0. dog1. cat2. mouse
