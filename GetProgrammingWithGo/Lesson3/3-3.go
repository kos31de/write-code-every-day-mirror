package main

import ("fmt")

func main() {
	var room = "cave"
	if room == "cave" {
		fmt.Println("ここは洞窟です")
	} else if room == "entrance" {
		fmt.Println("ここは入り口です")
	} else if room == "mountain" {
		fmt.Println("ここは山です")
	}
}
