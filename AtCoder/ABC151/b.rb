N, K, M = gets.chomp.split(" ").map(&:to_i)
A = gets.chomp.split(" ").map(&:to_i)

total = A.sum.to_i
target = (N * M).to_i

last_point = (target - total).to_i

if total >= target
  puts 0
elsif target - total > K
  puts (-1)
else
  puts last_point
end
