N = gets.chop.to_i
A = gets.chop.split.map(&:to_i)

# ユニークかどうか真偽値を返す
def uniq?(array)
  uniq_check = {}
  array.each do |v|
    return false if uniq_check.key? v
    uniq_check[v] = true
  end
  return true
end

if uniq?(A)
  puts "YES"
else
  puts "NO"
end
