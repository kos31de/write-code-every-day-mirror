# Rubyの標準入力

## 1文字ずつ複数行

```
a1
a2
.
.
aN
```

```ruby
n = gets.chomp.to_i
a = []
n.times { a << gets.to_i }
```

## 1行に複数文字

X Y Z

```ruby
x, y, z = gets.chomp.split.map(&:to_i)
```

## 1行に複数文字を配列に入れる

a1 a2 .... aN

```ruby
a = gets.chomp.split.map(&:to_i)
```

## 1行にスペース区切りで2文字ずつ、異なる2つの配列に入れていく

```
N
a1  b1
a2  b2
.  .
.  .
aN bN
```

```ruby
n = gets.chop.to_i
a = []
b = []
n.times do
  line = gets.chop.split.map(&:to_i)
  a << line.shift
  b << line
end
```
## TIPS
- `(a * b) % 2 == 0 ? print("Even\n") : print("Odd\n")` 三項演算子よく使う。
- Ruby2.3.3なので`sum`は使えないので`inject(:+)`
- 桁溢れが起きるときは`to_i`する

## 自作メソッド
```ruby
# 配列の中の数字がユニークかどうか真偽値を返す
def uniq?(array)
  uniq_check = {}
  array.each do |v|
    return false if uniq_check.key?(v)
    uniq_check[v] = true
  end
  return true
end
```
