# countはLだったら1増える、そうでなければ1減る
# countが0になるときはLとRの数が等しいからtotalを1増やす
def balanced_string_split(s)
  total = 0
  count = 0
  s.split("").each do |x|
      x == "L" ? count += 1 :  count -= 1
      total += 1 if count == 0 
  end
  total
end
