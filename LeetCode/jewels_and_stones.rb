def num_jewels_in_stones(j, s)
  j.downcase.squeeze
  s.count(j)
end
