# 配列の後ろのX座標から配列の前のX座標を引いた値と、配列の後ろのY座標から配列の前のY座標を引いた値のうち、大きい方が時間になる
def min_time_to_visit_all_points(points)
  time = 0
  (1...points.length).each { |i|
    time += [(points[i-1][0]-points[i][0]).abs, (points[i-1][1]-points[i][1]).abs].max
  }
  time
end
