def subtract_product_and_sum(n)
    n_product = n.to_s.chars.map(&:to_i).inject(:*)
    n_sum = n.to_s.chars.map(&:to_i).sum
    n_product - n_sum
end
